module ApplicationHelper
    
    def full_title(title="")
        base_title="My Homepage"
        if title.empty?
            base_title
        else
            base_title + " | " + title
        end
    end
    
end
