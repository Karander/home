class ProjectsController < ApplicationController
    before_action :logged_in_user
    
    def new
        @project=Project.new
    end
    
    def create
        @project=current_user.projects.build(project_params)
        if @project.save
            flash[:success]="Project added"
            redirect_to about_me_path
        else
            flash.now[:danger]="Adding project failed"
            render "new"
        end
    end
    
    def destroy
        Project.find(params[:id]).destroy
        @project_to_remove=params[:id]
        flash[:success]="Project removed"
        respond_to do |format|
            format.html { redirect_to about_me_path }
            format.js
        end
    end
    
    private
    
    def project_params
        params.require(:project).permit(:name, :description, :link)
    end
    
end
