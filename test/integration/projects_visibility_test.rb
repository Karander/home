require 'test_helper'

class ProjectsVisibilityTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:page_owner)
  end
  
  test "visibility for not loged in users" do
    get about_me_path
    assert_match "Log in", response.body
    assert_match "test_project_101", response.body
    assert_no_match "Edit", response.body
    assert_no_match "Remove", response.body
    assert_no_match "Add a project", response.body
  end
  
  test "visibility for loged in users" do
    log_in_as(@user)
    get about_me_path
    assert_match "Log out", response.body
    assert_match "test_project_101", response.body
    assert_match "Edit", response.body
    assert_match "Remove", response.body
    assert_match "Add a project", response.body
  end
  
end
