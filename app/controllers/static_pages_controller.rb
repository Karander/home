class StaticPagesController < ApplicationController
  def home
  end

  def contact
  end
  
  def about
  end
  
  def about_me
    @projects=Project.all.paginate(page: params[:page])
  end
  
end
