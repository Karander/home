require 'test_helper'

class ProjectsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:page_owner)
  end
  
  test "should redirect from new if not loged in" do
    get new_project_path
    assert_redirected_to login_path
  end
  
  test "should get new" do
    log_in_as(@user)
    get new_project_path
    assert_response :success
    assert_select "title", "My Homepage | Add a project"
  end
end
