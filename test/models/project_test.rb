require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  
  def setup
    @user=users(:page_owner)
    @project=@user.projects.build(name: "test 1", description: "testing this thing here", link:"link_to_the_test")
  end
  
  test "should be valid" do
    assert @project.valid?
  end
  
  test "should need a name" do
    @project.name=nil
    assert_not @project.valid?
  end
  
  test "should need a description" do
    @project.description=nil
    assert_not @project.valid?
  end
  
  test "should need a link" do
    @project.link=nil
    assert_not @project.valid?
  end
  
  test "should need a user" do
    @project.user_id=nil
    assert_not @project.valid?
  end
    
end
