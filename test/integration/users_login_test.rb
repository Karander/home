require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:page_owner)
  end
  
  test "login with invalid information" do
    get login_path
    assert_template "sessions/new"
    post login_path, params: { session: { name: "  ", passowrd: "  " } }
    assert_template "sessions/new"
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
  
  test "login and logout with valid information" do
    get root_path
    assert_match "Log in", response.body
    get login_path
    log_in_as(@user)
    assert_redirected_to root_path
    follow_redirect!
    assert is_logged_in?
    assert_match "Log out", response.body
    delete logout_path
    assert_not is_logged_in?
  end
  
end
